package com.ribamarmjs.githubjavapop.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ribamarmjs.githubjavapop.R;
import com.ribamarmjs.githubjavapop.listener.OnPullRequestClickListener;
import com.ribamarmjs.githubjavapop.model.PullRequest;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by ribamarmjs on 13/10/17.
 */

public class PullRequestAdapter extends RecyclerView.Adapter<PullRequestAdapter.VH> {

    private List<PullRequest> mPullList;
    private Context mContext;
    private OnPullRequestClickListener mPullRequestCLickListener;

    public PullRequestAdapter(List<PullRequest> list, Context context){
        this.mContext = context;
        this.mPullList = list;
    }

    public void setmPullRequestCLickListener(OnPullRequestClickListener cLickListener){
        this.mPullRequestCLickListener = cLickListener;
    }

    @Override
    public PullRequestAdapter.VH onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_pull, parent, false);
        VH viewHolder = new VH(view);
        view.setTag(viewHolder);
        view.setOnClickListener(mClickListener);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PullRequestAdapter.VH holder, int position) {
        PullRequest pull = mPullList.get(position);
        holder.txtTitlePullRequest.setText(pull.title);
        holder.txtBodyPullRequest.setText(pull.body);
        holder.txtUserName.setText(pull.user.login);
        holder.txtDatePull.setText(pull.createdAt.substring(0,10));
        Picasso.with(mContext)
                .load(pull.user.avatarUrl)
                .placeholder(R.mipmap.ic_place_holder_avatar)
                .into(holder.imgUserAvatar);
    }

    @Override
    public int getItemCount() {
        return mPullList.size();
    }

    private View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            VH vh = (VH) view.getTag();
            int position = vh.getAdapterPosition();
            PullRequest pull = mPullList.get(position);
            mPullRequestCLickListener.onRepoClick(pull,position);
        }
    };

    public class VH extends RecyclerView.ViewHolder {
        TextView txtTitlePullRequest;
        TextView txtBodyPullRequest;
        TextView txtUserName;
        ImageView imgUserAvatar;
        TextView txtDatePull;

        public VH(View itemView){
            super(itemView);
            txtTitlePullRequest = (TextView) itemView.findViewById(R.id.txtTitlePullRequest);
            txtUserName = (TextView) itemView.findViewById(R.id.txtUserName);
            txtBodyPullRequest = (TextView) itemView.findViewById(R.id.txtBodyPullRequest);
            imgUserAvatar = (ImageView) itemView.findViewById(R.id.imgUserAvatar);
            txtDatePull = (TextView) itemView.findViewById(R.id.txtDatePullRequest);
        }
    }
}
