package com.ribamarmjs.githubjavapop.listener;

import com.ribamarmjs.githubjavapop.model.PullRequest;

/**
 * Created by ribamarmjs on 13/10/17.
 */

public interface OnPullRequestClickListener {
    void onRepoClick(PullRequest repo, int position);
}
