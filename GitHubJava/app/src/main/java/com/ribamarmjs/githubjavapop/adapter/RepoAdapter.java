package com.ribamarmjs.githubjavapop.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ribamarmjs.githubjavapop.R;
import com.ribamarmjs.githubjavapop.listener.OnRepoCLickListener;
import com.ribamarmjs.githubjavapop.model.Repo;
import com.squareup.picasso.Picasso;


import java.util.List;

/**
 * Created by ribamarmjs on 10/10/17.
 */

public class RepoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Repo> mRepos;
    private Context mContext;
    private OnRepoCLickListener mRepoCLickListener;
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADYNG = 1;

    public RepoAdapter(List<Repo> list, Context context){
        this.mContext = context;
        this.mRepos = list;
    }

    @Override
    public int getItemViewType(int position) {
        //return super.getItemViewType(position);
        //return mRepos.get(position) == null ? VIEW_TYPE_LOADYNG: VIEW_TYPE_ITEM;
        if( mRepos.get(position) == null){
            Log.d("...", "veio nulo");
            return VIEW_TYPE_LOADYNG;
        }else{
            return VIEW_TYPE_ITEM;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        RecyclerView.ViewHolder viewHolder = null;
        if( viewType == VIEW_TYPE_ITEM){
            view = LayoutInflater.from(mContext).inflate(R.layout.item, parent, false);
            viewHolder = new VH(view);
            view.setTag(viewHolder);
            view.setOnClickListener(mClickListener);
        }else if ( viewType == VIEW_TYPE_LOADYNG ){
            view = LayoutInflater.from(mContext).inflate(R.layout.loading_item,parent, false);
            viewHolder = new VHLoading(view);
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if( holder instanceof VH) {
            VH holderItem = (VH) holder;
            Repo repo = mRepos.get(position);
            holderItem.txtRepoName.setText(repo.name);
            holderItem.txtRepoDescription.setText(repo.description);
            holderItem.txtQtdFork.setText(String.valueOf(repo.forks));
            holderItem.txtQtdStar.setText(String.valueOf(repo.stargazersCount));
            holderItem.txtUserName.setText(repo.owner.login);
            Picasso.with(mContext)
                    .load(repo.owner.avatarUrl)
                    .placeholder(R.mipmap.ic_place_holder_avatar)
                    .into(holderItem.imgAvatar);
        }else if ( holder instanceof VHLoading){
            VHLoading loading = (VHLoading) holder;
            loading.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return mRepos == null ? 0 : mRepos.size();
    }

    private View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            VH vh = (VH) view.getTag();
            int position = vh.getAdapterPosition();
            Repo repo = mRepos.get(position);
            mRepoCLickListener.onRepoClick(repo,position);
        }
    };

    public void setRepoCLickListener(OnRepoCLickListener cLickListener){
        this.mRepoCLickListener = cLickListener;
    }

    public class VH extends RecyclerView.ViewHolder {
            TextView txtRepoName;
            TextView txtRepoDescription;
            TextView txtQtdFork;
            TextView txtQtdStar;
            TextView txtUserName;
            ImageView imgAvatar;

            public VH(View itemView){
                super(itemView);
                txtRepoName = (TextView) itemView.findViewById(R.id.nameRepo);
                txtUserName = (TextView) itemView.findViewById(R.id.txtUserName);
                txtRepoDescription = (TextView) itemView.findViewById(R.id.descriptionRepo);
                txtQtdFork = (TextView) itemView.findViewById(R.id.txtQtdFork);
                txtQtdStar = (TextView) itemView.findViewById(R.id.txtQtdStar);
                imgAvatar = (ImageView) itemView.findViewById(R.id.imgAuthorAvatar);
            }
    }

    public class VHLoading extends RecyclerView.ViewHolder{
        ProgressBar progressBar;

        public VHLoading(View item){
            super(item);
            progressBar = (ProgressBar) item.findViewById(R.id.prgbItem);
        }
    }
}
