package com.ribamarmjs.githubjavapop.model;

/**
 * Created by ribamarmjs on 10/10/17.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RepoWrapper {

    @SerializedName("items")
    @Expose
    public List<Repo> items = null;

}
