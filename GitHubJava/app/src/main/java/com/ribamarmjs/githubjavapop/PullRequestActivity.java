package com.ribamarmjs.githubjavapop;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.ribamarmjs.githubjavapop.adapter.PullRequestAdapter;
import com.ribamarmjs.githubjavapop.listener.OnPullRequestClickListener;
import com.ribamarmjs.githubjavapop.model.PullRequest;
import com.ribamarmjs.githubjavapop.model.Repo;
import com.ribamarmjs.githubjavapop.network.GitHubJavaAPIService;
import com.ribamarmjs.githubjavapop.util.Util;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class PullRequestActivity extends AppCompatActivity {

    private List<PullRequest> mList = new ArrayList<>();
    private RecyclerView rvListPull;
    private PullRequestAdapter mAdapter;
    private Repo repo;
    private Toolbar toolBar;
    private View mEmptyView;


    private OnPullRequestClickListener mClickListener = (pullRequest, position) -> {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(pullRequest.htmlUrl));
        startActivity(i);

    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_request);

        toolBar= (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolBar);

        rvListPull = (RecyclerView) findViewById(R.id.rvListPullRequest);

        LinearLayoutManager mLayoutManager;
        mLayoutManager = new LinearLayoutManager(this);
        rvListPull.setLayoutManager(mLayoutManager);

        rvListPull.addItemDecoration(new DividerItemDecoration(rvListPull.getContext(),
                DividerItemDecoration.VERTICAL));

        mEmptyView    = findViewById(R.id.empty_result);
        mEmptyView.setVisibility(View.GONE);
        repo = getIntent().getParcelableExtra(Util.EXTRA_PULL);

        mAdapter = new PullRequestAdapter(mList,getApplicationContext());
        mAdapter.setmPullRequestCLickListener(mClickListener);
        rvListPull.setAdapter(mAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(repo != null){
            carregarPullRequest(repo.owner.login,repo.name);
        }

    }

    private void carregarPullRequest(String criador, String repositorio) {
        if( Util.isNetworkAvailable(this)) {
            Retrofit retrofit = new Retrofit.Builder()
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(Util.BASE_URL)
                    .build();
            GitHubJavaAPIService apiService = retrofit.create(GitHubJavaAPIService.class);

            Observable<List<PullRequest>> observable = apiService.getRepoPull(criador, repositorio);

            observable.subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnError( error -> {
                        Log.d("...",error.getMessage());
                    })
                    .subscribe( response -> {
                        if( response.isEmpty() ){
                            mEmptyView.setVisibility(View.VISIBLE);
                            rvListPull.setVisibility(View.GONE);
                        }else{
                            mEmptyView.setVisibility(View.GONE);
                            rvListPull.setVisibility(View.VISIBLE);
                            mList.addAll(response);
                            mAdapter.notifyDataSetChanged();
                        }

                    });
        }else{
            mEmptyView.setVisibility(View.VISIBLE);
            rvListPull.setVisibility(View.GONE);
        }

    }
}
