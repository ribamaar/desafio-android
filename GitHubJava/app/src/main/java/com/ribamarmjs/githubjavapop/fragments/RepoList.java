package com.ribamarmjs.githubjavapop.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ribamarmjs.githubjavapop.R;
import com.ribamarmjs.githubjavapop.adapter.RepoAdapter;

public class RepoList extends Fragment {

    private RepoAdapter mAdapter;
    private RecyclerView mRecyclerView;

    public RepoList() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_repo_list, container, false);
    }

}
