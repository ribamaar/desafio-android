package com.ribamarmjs.githubjavapop;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.ribamarmjs.githubjavapop.adapter.RepoAdapter;
import com.ribamarmjs.githubjavapop.listener.OnRepoCLickListener;
import com.ribamarmjs.githubjavapop.model.Repo;
import com.ribamarmjs.githubjavapop.model.RepoWrapper;
import com.ribamarmjs.githubjavapop.network.GitHubJavaAPIService;
import com.ribamarmjs.githubjavapop.util.Util;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {
    private DrawerLayout mDrawer;
    private Toolbar toolbar;
    private NavigationView nvDrawer;
    private ActionBarDrawerToggle drawerToggle;
    private RecyclerView rvListContainer;
    private RepoAdapter mAdapter;
    private List<Repo> mList = new ArrayList<>();
    private View mEmptyView;


    private int page;

    private OnRepoCLickListener mClickListener = (repo, position) -> {
        Intent it = new Intent(getBaseContext(), PullRequestActivity.class);
        it.putExtra(Util.EXTRA_PULL, repo);
        startActivity(it);
    };

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        page = 1;
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerToggle = setupDrawerToggle();
        mDrawer.addDrawerListener(drawerToggle);

        final ActionBar actionBar = getSupportActionBar();

        if( actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
        }



        rvListContainer = (RecyclerView) findViewById(R.id.rvListContainer);
        mEmptyView    = findViewById(R.id.empty_result);
        mEmptyView.setVisibility(View.GONE);

        LinearLayoutManager mLayoutManager;
        mLayoutManager = new LinearLayoutManager(this);
        rvListContainer.setLayoutManager(mLayoutManager);

        rvListContainer.addItemDecoration(new DividerItemDecoration(rvListContainer.getContext(),
                DividerItemDecoration.VERTICAL));

        rvListContainer.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                 boolean loading = true;
                int pastVisiblesItems, visibleItemCount, totalItemCount;
                if(dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (loading)
                    {
                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            loading = false;
                            load(++page);

                        }
                    }
                }
            }
        });
        mAdapter = new RepoAdapter(mList,getApplicationContext());
        mAdapter.setRepoCLickListener(mClickListener);

        rvListContainer.setAdapter(mAdapter);

    }

    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(
                this,
                mDrawer,
                R.string.drawer_open,
                R.string.drawer_close);
    }

    @Override
    protected void onResume() {
        super.onResume();
        load(page);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                mDrawer.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void load(int page){
        if( Util.isNetworkAvailable(this)) {
            Retrofit retrofit = new Retrofit.Builder()
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(Util.BASE_URL)
                    .build();
            GitHubJavaAPIService apiService = retrofit.create(GitHubJavaAPIService.class);

            rx.Observable<RepoWrapper> observable = apiService.getRepoList(page);

            observable.subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnError(error -> {
                        Log.d("...", error.getMessage());
                    })
                    .subscribe(response -> {
                        if (response.items.size() > 0) {
                            mEmptyView.setVisibility(View.GONE);
                            rvListContainer.setVisibility(View.VISIBLE);
                            mList.addAll(response.items);
                            mAdapter.notifyDataSetChanged();
                        } else {
                            mEmptyView.setVisibility(View.VISIBLE);
                            rvListContainer.setVisibility(View.GONE);
                        }
                    });
        }else{
            mEmptyView.setVisibility(View.VISIBLE);
            rvListContainer.setVisibility(View.GONE);
        }
    }
}


