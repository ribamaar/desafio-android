package com.ribamarmjs.githubjavapop.network;

import com.ribamarmjs.githubjavapop.model.PullRequest;
import com.ribamarmjs.githubjavapop.model.RepoWrapper;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by ribamarmjs on 11/10/17.
 */

public interface GitHubJavaAPIService {

    @GET("search/repositories?q=language:Java&sort=stars")
    Observable<RepoWrapper> getRepoList(@Query("page") int page);

    @GET("repos/{criador}/{repositorio}/pulls")
    Observable<List<PullRequest>> getRepoPull(@Path("criador") String criador, @Path("repositorio") String repo);
}
