package com.ribamarmjs.githubjavapop.listener;

import com.ribamarmjs.githubjavapop.model.Repo;

/**
 * Created by ribamarmjs on 13/10/17.
 */

public interface OnRepoCLickListener {
    void onRepoClick(Repo repo, int position);
}
